function handler(req, res) {
    res.writeHead(200, {
        'Content-Type': 'text/plain'
    });
    res.end('OpenShift Up And Rebuilt');
}

var http = require('http').createServer(handler).listen(8080);